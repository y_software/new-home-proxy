[![Pipeline Status](https://gitlab.com/Y_Software/new-home-proxy/badges/master/pipeline.svg)](https://gitlab.com/Y_Software/new-home-proxy/pipelines)
[![creates.io](https://img.shields.io/crates/v/new-home-proxy.svg?style=flat-square&logo=rust)](https://crates.io/crates/new-home-proxy)

# New Home: Proxy

Supplies a basic proxy for use with the New Home system.
Used to make the Core part publicly available with SSL encryption.
Enables the New Home UI to be hosted SSL encrypted and built as PWA.

## Setup

### Client

You can simply install the client by running `sudo make install-client` in the 
root folder of the repository. For the most part, running `sudo systemctl start new-home-proxy-client` 
and `sudo systemctl enable new-home-proxy-client` will suffice to get the client
running and keep it running. For setting it up in the UI you will have to switch
the connection type to "Proxy", enter the URL of the proxy server (assuming that
you use the public one this is https://new-home-proxy.herokuapp.com/). You can get
the "Client Name" from the `sudo systemctl status new-home-proxy-client` command,
you should get in the output somewhere the part "Client your-name-here starting!".

For the username and password part you will have to create a directory called 
"resources" inside the "/etc/new-home-proxy-client" directory. Inside this directory
you will have to create a file called "users.yaml". This needs to contain something
like this:

```yaml
- name: your_name
  password: password
  hashing_method: Plaintext

# OR

- name: your_securer_name
  password: $2y$10$1mSNRoMfwb7c8Ik2KJBvNeGRYurU/Gx6JqJF3qKeMD9u/mQ6VIsJW
  hashing_method: BCrypt
```

The name and password fields are self-explaining, but as little hint for the "hashing_method"
field: 
The valid values here are (for now) "Plaintext" and "BCrypt". Plaintext causes the password to
be treated like a plain text string. So there is no encryption or security involved (within 
this file). This will most of the time be enough for you, but if you want to be sure, that no one
who could get the file knows your password, take the "BCrypt" variant. With this type the "password"
field has to contain a password encrypted with the BCrypt algorithm. You can find a generator for 
this [here](https://bcrypt-generator.com/). **But keep in mind, that this may slow down your requests.**

### Server

#### Heroku

Setting up the proxy with Heroku is very easy. You only have to

- [create an account](https://signup.heroku.com/) (No credit card or any other payment required **by now**)
- [create a new app](https://dashboard.heroku.com/new-app)
- Setup the Heroku CLI (an official how to you can find [here](https://devcenter.heroku.com/articles/heroku-cli#npm))
- Execute the command `heroku git:remote --app new-home-proxy` inside this git directory
- Push the app with the `git push heroku`

Done!

The last command may take a while when initially setting up the app as heroku will have to download all the 
dependencies but this will settle down for future deployments/updates of the proxy.

The good things about the Heroku method are:

- it is free
- you don't have to think about SSL

The free part may not seem too important, but if you're new to infrastructure, the SSL part may hurt you.

#### With Linux + Systemd

You can simply install the server by running `sudo make install-server` in the 
root folder of the repository. This command will build and install all the server related files.
The server can then be started with the command `sudo systemctl start new-home-proxy-client` and
be added to the autostart with the `sudo systemctl enable new-home-proxy-client` command.

Now comes the hard(er) part. As the proxy server is not having SSL built-in you have to set up a reverse proxy 
for it. This can be easily done with nginx or Apache. 

To summarize before starting, you have to have the following things:

- The New Home Proxy (server) installed
- The New Home Proxy running
- Another HTTP reverse proxy (Like nginx)
- A domain (for the certificate)
- An SSL certificate (for the domain)

The certificate needs to be a signed one as some browsers may not allow requests to sites with an unsigned certificate
from sites with a signed one (in case of the public UI, which has a signed certificate, the browser would reject the
request to your unsigned proxy).

The domain part you have to find out yourself, there is no "golden-path" for that, but for the  signed certificate 
you can try out letsencrypt (which works well if you get it set up). You can find instructions for your system and
web server (I will use nginx in this documentation tho) [here](https://certbot.eff.org/instructions). There you can
pick your distribution and web server, and the will give you a solution.

Now we're starting with setting up the reverse proxy. The most basic proxy configuration with SSL looks like the
following one:

```nginx
server {
    listen 0.0.0.0:443 ssl;
    server_name your-proxy-domain.example.com; // Fill out your domain here

    ssl_certificate /etc/nginx/ssl/certificate.pem; // Fill out your certificate here
    ssl_certificate_key /etc/nginx/ssl/certificate-key.pem; // Fill out your certificate key here

    location / {
        proxy_pass_request_headers on;
        proxy_pass http://127.0.0.1:5355; // This is the address on which the proxy runs, keep in mind changing this if you have changed the proxy's config
        proxy_read_timeout 90;

        proxy_redirect http://127.0.0.1:5355 /; // Proxy address
    }
}
```

## Uninstall



## Why?

While you still can run your own UI, I want to have the UI available as PWA. 
For this however I have to add a Service Worker to it and this on the other 
hand requires the UI to be served via HTTPS (which still is possible for me).
But now the Core has to be reachable as well via HTTPS, and this certificate 
has to be trusted. This is where I fail. No one will give you a (trusted) SSL
certificate for an IP, especially not for a network-internal IP. So I have to 
get the Core out of your home and available via SSL. So this proxy server will
be hosted (with SSL encryption) by me or if you have the know-how by yourself.

## How? (Tech part)

The proxy constits of 2 parts:

* The client, which will run on your Raspberry PI
* The server, which will run on my (or your) Server

Additionally, I will provide configuration files, so that you can deploy it
straight to Heroku and have this proxy hosted "by yourself". At least so that
you know, that I'm not copying data on my server.

### How the connection works

Giving the public access to your home is a sensible topic, so it has to be secure.
I try my best to make it secure by encrypting all the traffic between your home,
the proxy server and the UI (aka your local device). To ensure that not someone
else will access your home resources, the client proxy requires a login. The login
is not part of the server but on the client (**your** Raspberry PI).

To get away without any port forwarding in your router, the client connects to the
server. By this only the server has to be available in the public and the client
is just sitting some where, even behind a firewall. And the even better part is,
that the communication runs via websocket connection, so you only need a single
port and a single SSL setup for securing the proxy request and the client tunnel.

To secure your home from intruders there are login credentials that has to be sent
in a proxy request. They will be forwarded to the client and the client will then
validate them. By this you can't be fooled by a manipulated server that is allowing
everything.

All of this should prevent someone from breaking into your home. If someone has 
still concerns please let me know of this, so I can get into it and try to fix this.

