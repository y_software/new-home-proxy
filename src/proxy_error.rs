use std::error::Error;
use std::fmt::{Debug, Display, Result};
use std::io;
use std::result;
use std::sync::mpsc::{RecvTimeoutError, SendError};

use serde::export::Formatter;

use crate::communication::ProxyResponse;

/// The basic result type filled with the `ProxyError`
pub type ProxyResult<T> = result::Result<T, ProxyError>;

/// This is the main error type inside the proxy.
/// This error can be create from all other errors (that are handled in the application)
pub struct ProxyError {
    message: String,
    debug: String,
}

impl ProxyError {
    pub fn new(message: String, debug: String) -> Self {
        Self { message, debug }
    }

    pub fn message(message: impl ToString) -> Self {
        Self {
            debug: message.to_string().clone(),
            message: message.to_string().clone(),
        }
    }
}

impl Error for ProxyError {}

impl Display for ProxyError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> Result {
        formatter.write_str(&self.message)
    }
}

impl Debug for ProxyError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> Result {
        formatter.write_str(&self.debug)
    }
}

impl From<io::Error> for ProxyError {
    fn from(parent: io::Error) -> Self {
        Self {
            message: format!("{}", parent),
            debug: format!("{:?}", parent),
        }
    }
}

impl From<serde_json::error::Error> for ProxyError {
    fn from(parent: serde_json::error::Error) -> Self {
        Self {
            message: format!("{}", parent),
            debug: format!("{:?}", parent),
        }
    }
}

impl From<serde_yaml::Error> for ProxyError {
    fn from(parent: serde_yaml::Error) -> Self {
        Self {
            message: format!("{}", parent),
            debug: format!("{:?}", parent),
        }
    }
}

impl From<SendError<ProxyResponse>> for ProxyError {
    fn from(parent: SendError<ProxyResponse>) -> Self {
        Self {
            message: format!("{}", parent),
            debug: format!("{:?}", parent),
        }
    }
}
impl From<RecvTimeoutError> for ProxyError {
    fn from(parent: RecvTimeoutError) -> Self {
        Self {
            message: format!("{}", parent),
            debug: format!("{:?}", parent),
        }
    }
}
