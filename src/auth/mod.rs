//!
//! Here we're taking care of user authentication. Users will be stored in an extra yaml file.
//!
//! ## The users file
//!
//! The users file will contain an array of users which are (by now) only a name and a password.
//! This password is a bcrypt hashed string so if the password file goes public still no one can
//! simply read the password. However this also means that if you lose your password you cannot
//! just read it from somewhere but you will have to set a new password
//!
//! ```yaml
//! - username: Admin
//!   password: admin
//!   hashing_method: HashMethod
//!
//! - username: Second user
//!   password: unknown
//!   hashing_method: HashMethod
//! ```
//!

/// **[WIP]**: Contains user types
pub mod user;

/// Authenticates the users
pub mod user_authenticator;
