//!
//! This module contains user structs and traits
//!

use serde::{Deserialize, Serialize};

/// This enum provides the available hashing methods.
/// The hashing method indicate how the user's password is encrypted/hashed. This may change in future
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum HashingMethod {
    /// Hashes the password with the bcrypt algorithm
    BCrypt,

    /// This is not hashing the password at all but just storing it in plaintext
    Plaintext,
}

/// This is the type which is stored inside the users file. Aka. an array of users
pub type Users = Vec<User>;

/// This contains the bare information of a user which is for now only the username and the password
/// **Note: It is not planed to add any permission information here**
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct User {
    pub name: String,
    pub password: String,
    pub hashing_method: HashingMethod,
}
