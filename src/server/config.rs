//! # Example Config
//!
//! ```yaml
//! # The address on which the server is bound
//! # this defaults to the IPv6 notation for all interfaces (`[::]`)
//! bind_address: "[::]"
//!
//! # The address on which the server listens for incoming connections
//! # This port is used for the connect **and** proxy requests as this proxy runs purely with http/ws
//! # technology
//! bind_port: 13370
//! ```
//!

use std::fs::File;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::proxy_error::ProxyResult;

/// This config struct contains all properties that are required for proxy server to run
#[derive(Serialize, Deserialize)]
pub struct Config {
    /// The address on which the proxy server listens for connection (ws) and proxy (http) requests
    pub bind_address: String,

    /// The port on which the proxy server listens for connection (ws) and proxy (http) requests
    pub bind_port: u16,
}

impl Config {
    pub fn initialize(file: PathBuf) -> ProxyResult<Self> {
        if !file.exists() {
            let file = File::create(file.clone())?;

            serde_yaml::to_writer(file, &Self::default())?;
        }

        Self::load_from_file(file)
    }

    fn load_from_file(file: PathBuf) -> ProxyResult<Self> {
        let file = File::open(file)?;

        Ok(serde_yaml::from_reader(file)?)
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            bind_address: String::from("[::]"),
            bind_port: 5355,
        }
    }
}
