/// This module contains the config struct for the server application
pub mod config;

/// This module handles the websocket requests and responses
pub mod websocket_handler;

/// This module contains code to control and routes requests to clients
pub mod client_manager;

/// This module contains the http request handler for the server
pub mod handler;
