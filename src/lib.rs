//! # New Home Proxy
//!
//! This library contains shared code for the two contained binaries "client" and "server".
//!
//! New Home is a project for creating your own smart home with open code and low cost in the way of
//! hardware as it runs on potentially all systems (Raspberry PI, ***any-other-fruit-or-thing***-PI,
//! Arduino/ESP/NodeMCU microcontrollers).
//! The Proxy is used to access New Home resources from outside of your home.
//!
//! ## Proxy server
//!
//! The server is the part that can be publicly available and route requests to multiple clients.
//! It keeps your requests secure with SSL (if hosted correctly).
//!
//! ### In combination with the UI
//!
//! The proxy was intentionally created to work together with the "New Home UI" which *can* supply a
//! PWA, **but only** if it is served via HTTPS which would require the Core to also be reachable via
//! HTTPS which can be complicated to setup. So this proxy grants you access to the PWA and allows
//! you to control your home from everywhere.
//!
//! ### Config
//!
//! You can find a description for the fields and an example config in the
//! [Config](new_home_proxy::server::config::Config) struct
//!
//! ## Proxy client
//!
//! Runs on your New Home controller device and establishes a connection to the server. By this you
//! dont have to fight with port-forwarding or anything else.
//!
//! ### In combination with the Core
//!
//! As said, this proxy is meant to be used with the "New Home Core". The client takes the role of
//! receiving requests from the server and forwarding it to the Core while also make sure that the
//! request is authorized (with a username and a password).
//!
//! ### Config
//!
//! You can find a description for the fields and an example config in the
//! [Config](new_home_proxy::client::config::Config) struct
//!
//! ## How it works
//!
//! When started the client will generate a name for itself. This name is used to connect to the
//! configured proxy then.
//!
//! The connection is done through websockets. This allows you to just start the client and don't
//! care about port forwarding, daily changing IPs etc.
//!

extern crate actix_web;
extern crate bcrypt;
extern crate serde;
extern crate serde_yaml;
#[macro_use]
extern crate serde_json;
extern crate actix_codec;
extern crate awc;
extern crate futures;
extern crate futures_util;
extern crate percent_encoding;

/// This module contains all the code that is required to authorize user requests on the client side
pub mod auth;

/// This module contains the central error of the application
pub mod proxy_error;

/// This module contains server specific code
pub mod server;

/// This module contains structs for the communication between the server and client
pub mod communication;

/// This module contains client specific code
pub mod client;
