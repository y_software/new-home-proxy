/// This module contains all the code for the client config
pub mod config;

/// This contains all the handlers used in the client
pub mod handler;

/// This contains a name generator used in the config to generate the client_id field
pub mod name_generator;
