.PHONY: install
.PHONY: install-client
.PHONY: install-server

install: install-client

install-client:
	cargo build --release --bin=client
	install target/release/client /usr/bin/new-home-proxy-client
	install systemd/new-home-proxy-client.service /usr/lib/systemd/system

install-server:
	cargo build --release --bin=server
	install target/release/server /usr/bin/new-home-proxy-server
	install systemd/new-home-proxy-server.service /usr/lib/systemd/system

uninstall:
	rm -f /usr/bin/new-home-proxy-client /usr/bin/new-home-proxy-server
	rm -f /usr/lib/systemd/system/new-home-proxy-client /usr/lib/systemd/system/new-home-proxy-server
	rm -rf /etc/new-home-proxy-client /etc/new-home-proxy-server

